package com.hcl.bb.customException;

import org.apache.log4j.Logger;


public class UserDefinedException extends Exception{

	private String message;
	private static final Logger LOGGER = Logger.getLogger(UserDefinedException.class);
	
	public UserDefinedException(String message) {
		super();
		this.message = message;
		LOGGER.error("UserException: "+this.message);
	}

}
