package com.hcl.bb.controller;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;
import com.hcl.bb.model.User;
import com.hcl.bb.model.UserLogin;
import com.hcl.bb.service.AdminService;
import com.hcl.bb.service.EmailService;
import com.hcl.bb.service.PatientDonorService;
import com.hcl.bb.service.RequestPatientService;
import com.hcl.bb.service.UserService;

@Controller("patientController")
public class PatientController {

	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RequestPatientService requestPatientService; 
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private PatientDonorService patientDonorService; 
	
	@RequestMapping("user/login")
	public String userLogin(Model model) {
		model.addAttribute("userLogin",new UserLogin());
		return "login";
	}
	
	@PostMapping("user/authenticate")
	public String userData(@Valid @ModelAttribute("userLogin") UserLogin user,BindingResult bindingResult,Model model,HttpServletRequest request) throws UserDefinedException {
		
		if(bindingResult.hasErrors()) {
			return "login";
		}
		else {
			boolean check = userService.checkLogin(user.getUsername(), user.getPassword());
			if(check==true) {
				request.getSession().setAttribute("name", user.getUsername());
				return "redirect:/home";
			}
			else {
				model.addAttribute("error", "Invalid login details");
				return "login";
			}
		}
		
	}
	
	@RequestMapping("user/add")
	public String userAdd(Model model) {
		model.addAttribute("newUser", new User());
		return "userRegister";
	}
	
	@PostMapping("user/register")
	public ModelAndView userRegister(@Valid @ModelAttribute("newUser") User user,BindingResult bindingResult,Model model) throws UserDefinedException {
		
		ModelAndView modelAndView = new ModelAndView();
		if(bindingResult.hasErrors()) {
			modelAndView.setViewName("userRegister");
		}else {
			boolean check = userService.addUser(user);
			if(check==true) {
				modelAndView.setViewName("redirect:./login");
			}
		}
		return modelAndView;
	}
	
	
	/*@RequestMapping(value= "user/forgot", method=RequestMethod.GET)
	public String forgotPage() {
		return "forgotPassword";
	}
	
	@RequestMapping(value="user/forgot",method=RequestMethod.POST)
	public ModelAndView processforgotForm(ModelAndView modelAndView,@RequestParam("email") String email,HttpServletRequest request) throws UserDefinedException {
		
		User user = userService.findUserByEmail(email);
		if(user==null) {
			modelAndView.addObject("errorMessage", "We didn't find an account for the email address");
		}else {
			user.setResetToken(UUID.randomUUID().toString());
			
			userService.update(user);
			String url = request.getScheme() +"://" + request.getServerName();
			
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom("kiran.chenna12345@gmail.com");
			message.setTo(user.getEmail());
			message.setSubject("Forgot Password Reset Request");
			message.setText("To reset the password click the following link: "+url+ "/reset?token="+user.getResetToken());
			
			emailService.sendEmail(message);
			modelAndView.addObject("successMessage", "A password reset link has been sent to "+user.getEmail());
		}
		modelAndView.setViewName("forgotPassword");
		return modelAndView;
	}
	*/
	@RequestMapping("/home")
	public String homePage(HttpServletRequest request,Model model) {
		
		String name = (String)request.getSession().getAttribute("name");
		if(name==null) {
			model.addAttribute("error", "Session has ended or cannot view the page,should login first");
			return "error";
		}else {
		return "home";
		}
	}
	
	@GetMapping("/admin")
	public String adminLogin() {
		return "adminlogin";
	}
	
	@PostMapping("/adminPage")
	public String adminData(@RequestParam("username") String name,@RequestParam("password") String password) throws UserDefinedException {
		boolean check = adminService.checkAdmin(name, password);
		if(check==true) {
			return "adminHome";
		}
		return "adminlogin";
	}
	
	
	
	@GetMapping("/request")
	public String requestPage(Model model) {
		model.addAttribute("requestData", new PatientRequest());
		return "request";
	}
	
	@PostMapping("/requestform")
	public String requestStatus(@Validated @ModelAttribute("requestData") PatientRequest patientRequest,BindingResult bindingResult,Model model,HttpServletRequest request) throws UserDefinedException {
		
		if(bindingResult.hasErrors()) {
			return "request";
		}else {
			String name= (String)request.getSession().getAttribute("name");
			boolean check1  =requestPatientService.addRequestForm(patientRequest,name);
			if(check1==true) {
				return "redirect:/requeststatus";
			}
		}
		return "error";
	}
	
	@GetMapping("/patientrequests")
	public String adminResponses(Model model) throws UserDefinedException {
		
		List<PatientRequest> patientRequests = adminService.getAllRequests();
			model.addAttribute("patientRequests", patientRequests);
			return "requestStatusData";	
		
	}
	
	@GetMapping("/requeststatus")
	public String requestData(Model model,HttpServletRequest request) throws UserDefinedException  {
		
		String name = (String)request.getSession().getAttribute("name");
		User user = userService.findUserByName(name);
		System.out.println(user.getId());
		try {
			List<PatientRequest> userrequests = requestPatientService.getAllRequests(user.getId());
			if(!userrequests.isEmpty()) {
				model.addAttribute("requests", userrequests);
			}
		} catch (UserDefinedException e) {
			return "requestStatus";
		}
		return "requestStatus";

	}
	
	
	@PostMapping("/approvestatus/{value}")
	public String approveStatus(@PathVariable("value") Long id,Model model) throws UserDefinedException {
		
		
		String status="Approved";
		boolean check= adminService.updateRequestStatusById(id,status);
		String page;
		if(check==true) {
			System.out.println("status changed");
			return "redirect:/patientrequests";
		}
		else {
			page="error";
		}
		return page;
	}
	
	@PostMapping("/rejectstatus/{value}")
	public String rejectStatus(@PathVariable("value") Long id,Model model) throws UserDefinedException {
		
		
		String status="Rejected";
		boolean check= adminService.updateRequestStatusById(id,status);
		String page;
		if(check==true) {
			System.out.println("status changed");
			return "redirect:/patientrequests";
		}
		else {
			page="error";
		}
		return page;
	}
	
	
	
	
	@RequestMapping("/donor")
	public String donorPage(Model model) {
		
		model.addAttribute("donarData", new PatientDonor());
		return "donar";
	}
	
	
	@PostMapping("/donorform")
	public String donorStatus(@Validated @ModelAttribute("donarData") PatientDonor patientDonor,BindingResult bindingResult,Model model,HttpServletRequest request) throws UserDefinedException {
		
		if(bindingResult.hasErrors()) {
			return "donar";
		}else {
			String name= (String)request.getSession().getAttribute("name");
			System.out.println(name);
			boolean check1  = patientDonorService.addDonorForm(patientDonor, name);
			if(check1==true) {
				return "redirect:/donorstatus";
			}
		}
		return "error";
	}
	
	
	@GetMapping("/donorstatus")
	public String donorstatus(Model model,HttpServletRequest request)  {
		
		String name = (String)request.getSession().getAttribute("name");
		try {
			User user = userService.findUserByName(name);
			List<PatientDonor> userdonors = patientDonorService.getAllRequests(user.getId());
			if(!userdonors.isEmpty()) {
				model.addAttribute("donors", userdonors);
			}
		}catch(UserDefinedException e) {
			return "donorStatus";
		}
		return "donorStatus";
	}
	
	@GetMapping("/patientdonors")
	public String adminDonorReponse(Model model) throws UserDefinedException {
		
		List<PatientDonor> patientDonors = adminService.getAllDonors();
			model.addAttribute("patientDonors", patientDonors);
			return "donorStatusData";	
		
	}
	
	@PostMapping("/approvednstatus/{value}")
	public String approveDnStatus(@PathVariable("value") Long id,Model model) throws UserDefinedException {
		
		
		String status="Approved";
		boolean check= adminService.updateDonorStatusById(id,status);
		String page;
		if(check==true) {
			System.out.println("status changed");
			return "redirect:/patientdonors";
		}
		else {
			page="error";
		}
		return page;
	}
	
	@PostMapping("/rejectdnstatus/{value}")
	public String rejectDnStatus(@PathVariable("value") Long id,Model model) throws UserDefinedException {
		
		
		String status="Rejected";
		boolean check= adminService.updateDonorStatusById(id,status);
		String page;
		if(check==true) {
			System.out.println("status changed");
			return "redirect:/patientdonors";
		}
		else {
			page="error";
		}
		return page;
	}
	
	
	@RequestMapping("/availability")
	public String checkBlood(Model model) {
		try {
			List<User> users=patientDonorService.checkBloodStatus();
			model.addAttribute("bloodAvailUsers", users);
		} catch (UserDefinedException e) {
			return "bloodStatus";
		}
		return "bloodStatus";
	}
	
	
	@GetMapping("/logout")
	public String logoutPage(HttpServletRequest request) {
		HttpSession session  = request.getSession();
		session.invalidate();
		return "redirect:user/login";
	}
	
	
	
}
