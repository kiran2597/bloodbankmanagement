package com.hcl.bb.service;

import java.util.List;
import java.util.Optional;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.User;

public interface UserService {

	public abstract boolean addUser(User user) throws UserDefinedException;
	public abstract User findUserByName(String name) throws UserDefinedException;
	public abstract boolean checkLogin(String username,String password) throws UserDefinedException;
	public abstract List<User> listAllUsers() throws UserDefinedException;
	public abstract User findUserByEmail(String email) throws UserDefinedException;
	public abstract User findUserByResetToken(String resetToken) throws UserDefinedException;
	public abstract void update(User user) throws UserDefinedException;
}
