package com.hcl.bb.service;

import java.util.List;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;

public interface AdminService {


	public abstract List<PatientRequest> getAllRequests() throws UserDefinedException;
	public abstract boolean checkAdmin(String name,String password) throws UserDefinedException;
	public abstract boolean updateRequestStatusById(Long id,String status) throws UserDefinedException;
	public abstract boolean updateDonorStatusById(Long id,String status) throws UserDefinedException;
	public abstract List<PatientDonor> getAllDonors() throws UserDefinedException;
}
