package com.hcl.bb.service;

import java.util.List;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.PatientRequest;

public interface RequestPatientService {

	public abstract boolean addRequestForm(PatientRequest patientRequest,String name) throws UserDefinedException;
	public abstract List<PatientRequest> getAllRequests(Long userId) throws UserDefinedException;
}
