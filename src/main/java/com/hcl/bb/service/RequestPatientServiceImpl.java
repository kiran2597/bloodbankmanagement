package com.hcl.bb.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.dao.RequestPatientDao;
import com.hcl.bb.model.PatientRequest;

@Service("requestPatientService")
public class RequestPatientServiceImpl implements RequestPatientService{

	@Autowired
	private RequestPatientDao  requestPatientDao;
	
	@Override
	@Transactional
	public boolean addRequestForm(PatientRequest patientRequest,String name) throws UserDefinedException {
		
		boolean check = false;
		if(patientRequest!=null) {
			check = requestPatientDao.addRequestForm(patientRequest,name);
		}
		else {
			throw new UserDefinedException("PatientRequest form should not be empty with fields");
		}
		return check;
	}

	@Override
	@Transactional
	public List<PatientRequest> getAllRequests(Long userId) throws UserDefinedException {

		List<PatientRequest> requests = requestPatientDao.getAllRequests(userId);
		if(requests!=null) {
			return requests;
		}
		else {
			throw new UserDefinedException("No requests are from user");
		}
	}


}
