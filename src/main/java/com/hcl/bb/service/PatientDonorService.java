package com.hcl.bb.service;

import java.util.List;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.User;

public interface PatientDonorService {

	public boolean addDonorForm(PatientDonor donor, String name) throws UserDefinedException;
	public abstract List<PatientDonor> getAllRequests(Long userId) throws UserDefinedException;
	public abstract List<User> checkBloodStatus() throws UserDefinedException;
}
