package com.hcl.bb.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.dao.UserDao;
import com.hcl.bb.model.User;

@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Override
	public boolean addUser(User user) throws UserDefinedException {
		
		boolean check = false;
		if(user!=null) {
			check = userDao.addUser(user);
		}
		else {
			throw new UserDefinedException("User is not inserted");
		}
		return check;
	}

	@Override
	@Transactional
	public User findUserByName(String name) throws UserDefinedException {
		
		User user = null;
		if(name!=null) {
			user = userDao.findUserByName(name);
		}else {
			throw new UserDefinedException("Name should not be null/empty");
		}
		return user;
	}

	@Override
	@Transactional
	public boolean checkLogin(String username, String password) throws UserDefinedException {
	
		boolean check = false;
		if(username!=null && password!=null) {
			List<User> users = listAllUsers();
			for (User user2 : users) {
				if(user2.getUserName().equals(username) && user2.getPassword().equals(password)) {
					check=true;
				}
			}
		}
		else {
			throw new UserDefinedException("Either username or password is null");
		}
		return check;
	}

	@Override
	@Transactional
	public List<User> listAllUsers() throws UserDefinedException {
		
		List<User> userlist = userDao.listAllUsers();
		if(userlist!=null) {
			return userlist;
		}
		else {
			throw new UserDefinedException("No users present in database");
		}
	}

	@Override
	@Transactional
	public User findUserByEmail(String email) throws UserDefinedException {
		
		User userdetails = userDao.findByEmail(email);
		if(userdetails!=null) {
			return userdetails;
		}
		else {
			return null;
		}
	}

	@Override
	@Transactional
	public User findUserByResetToken(String resetToken) throws UserDefinedException{
		
		User userdetails = userDao.findByResetToken(resetToken);
		if(userdetails!=null) {
			return userdetails;
		}
		else {
			return null;
		}
	}

	@Override
	public void update(User user) throws UserDefinedException {
		
		if(user!=null) {
			boolean value = userDao.update(user);
			if(value==false) {
				throw new UserDefinedException("User details is not updated");
			}
		}
	}

}
