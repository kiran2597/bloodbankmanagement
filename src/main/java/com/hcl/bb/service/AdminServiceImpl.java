package com.hcl.bb.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.dao.AdminDao;
import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;

@Service("adminService")
public class AdminServiceImpl implements AdminService{

	@Autowired
	private AdminDao admindao;
	
	@Override
	@Transactional
	public List<PatientRequest> getAllRequests() throws UserDefinedException {
		
		List<PatientRequest> requests = admindao.getAllRequests();
		if(requests!=null) {
			return requests;
		}
		else {
			throw new UserDefinedException("No Patient requests is found");
		}
	}

	@Override
	@Transactional
	public boolean checkAdmin(String name, String password) throws UserDefinedException {
		
		boolean check=false;
		if(name!=null && password!=null) {
			check = admindao.checkAdmin(name, password);
		}else {
			throw new UserDefinedException("Details should not be empty");
		}
		return check;
	}

	@Override
	@Transactional
	public boolean updateRequestStatusById(Long id,String status) throws UserDefinedException {
		
		boolean check = admindao.updateRequestStatusById(id,status);
		boolean state=false;
		if(check==true) {
			state = true;
		}else {
			throw new UserDefinedException("Patient status is not updated..");
		}
		return state;
	}

	@Override
	@Transactional
	public List<PatientDonor> getAllDonors() throws UserDefinedException {
		
		List<PatientDonor> donors = admindao.getAllDonors();
		if(donors!=null) {
			return donors;
		}
		else {
			return null;
			//throw new UserDefinedException("No Patient donors are found");
		}
		
	}

	@Override
	@Transactional
	public boolean updateDonorStatusById(Long id, String status) throws UserDefinedException {
		
		boolean check = admindao.updateDonorStatusById(id,status);
		boolean state=false;
		if(check==true) {
			state = true;
		}else {
			throw new UserDefinedException("Patient status is not updated..");
		}
		return state;
	}

}
