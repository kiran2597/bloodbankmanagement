package com.hcl.bb.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.dao.PatientDonorDao;
import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;
import com.hcl.bb.model.User;


@Service("patientDonorService")
public class PatientDonorServiceImpl implements PatientDonorService{

	@Autowired
	private PatientDonorDao patientDonorDao; 
	
	@Override
	@Transactional
	public boolean addDonorForm(PatientDonor donor, String name) throws UserDefinedException {
		
		boolean check = false;
		
		if(donor!=null) {
			check = patientDonorDao.addDonorForm(donor, name);
		}
		else {
			throw new UserDefinedException("Donor data is not added");
		}
		return check;
	}

	@Override
	@Transactional
	public List<PatientDonor> getAllRequests(Long userId) throws UserDefinedException {
		
		List<PatientDonor> donors = patientDonorDao.getAllRequests(userId);
		if(donors!=null) {
			return donors;
		}
		else {			
			throw new UserDefinedException("No donors are from user");
		}
		
	}

	@Override
	public List<User> checkBloodStatus() throws UserDefinedException {
		
		List<User> users = patientDonorDao.checkBloodStatus();
		if(users!=null) {
			return users;
		}
		else {
			throw new UserDefinedException("No blood availability");
		}
	}

}
