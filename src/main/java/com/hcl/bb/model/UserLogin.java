package com.hcl.bb.model;

import javax.validation.constraints.NotEmpty;

public class UserLogin {

	@NotEmpty(message = "Username field should not be empty")
	private String username;
	@NotEmpty(message = "Password field should not be empty")
	private String password;
	
	
	public UserLogin() {
		super();
	}
	public UserLogin(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
