package com.hcl.bb.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@Entity(name ="user")
@Table(name = "user")
public class User implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userId")
	private Long id;
	
	@Column(name = "firstname",length=20,nullable=false)
	@NotEmpty(message="FirstName should not be Empty")
	private String firstName;
	
	@Column(name = "lastname",length=20,nullable=false)
	@NotEmpty(message="LastName should not be Empty")
	private String lastName;
	
	@Column(name = "username",length=20,nullable=false)
	@NotEmpty(message="UserName should not be Empty")
	private String userName;
	
	@Column(name="password",length=20,nullable=false)
	@Size(min=5, max=10, message="Password should have minimum 5 and maximum 10 characters")
	@NotEmpty(message="Password should not be Empty")
	private String password;
	
	@Column(name = "bloodGroup",length=3,nullable=false)
	@NotEmpty(message="BloodGroup should not be Empty")
	private String bloodGroup;
	
	@Column(name = "city",length=20,nullable=false)
	@NotEmpty(message="City should not be Empty")
	private String city;
	
	@Column(name = "phoneNumber",length=10,nullable=false)
	@Pattern(regexp="(^$|[0-9]{10})" , message="Enter valid Number")
	@NotEmpty(message="PhoneNumber should not be Empty")
	private String phoneNumber;

	@Column(name = "reset_token")
	private String resetToken;
	
	@Column(name="Email")
	@Email(message="Please provide valid email address")
	@NotEmpty(message="Email should not be empty")
	private String email;
	
	public User() {
		super();
	}

	public String getResetToken() {
		return resetToken;
	}
	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
}