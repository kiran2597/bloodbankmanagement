package com.hcl.bb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "patientRequest")
public class PatientRequest implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "requestId")
	private Long id;

	@Column(name = "patientName", length = 20, nullable = false)
	@NotEmpty(message = "PatientName shouldn't be empty")
	private String patientName;
	
	@Column(name = "bloodGroup", length = 20, nullable = false)
	@NotEmpty(message = "Bloodgroup shouldn't be empty")
	private String bloodGroup;
	
	@Column(name = "city", length = 20, nullable = false)
	@NotEmpty(message = "City shouldn't be empty")
	private String city;
	
	@Column(name = "doctorName", length = 20, nullable = false)
	@NotEmpty(message = "Doctorname shouldn't be empty")
	private String doctorName;
	
	@Column(name = "hospitalNameAddress", length = 100, nullable = false)
	@NotEmpty(message = "Hospital and address shouldn't be empty")
	private String hospitalNameAddress;
	
	@Column(name = "date", nullable = false)
	@NotNull(message="Date should not be empty")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date date;
	
	@Column(name = "contactName", length = 20, nullable = false)
	@NotEmpty(message = "Contactname shouldn't be empty")
	private String contactName;
	
	@Column(name = "contactNumber", length = 20, nullable = false)
	@NotEmpty(message = "Contactnumber shouldn't be empty")
	@Size(min=10,max=10)
	private String contactNumber;
	
	@Column(name = "contactEmailId", length = 20, nullable = false)
	@NotEmpty(message = "ContactEmailId shouldn't be empty")
	private String contactEmailId;
	
	@Column(name = "message", length = 100, nullable = false)
	@NotEmpty(message = "Message shouldn't be empty")
	private String message;

	@Column(name="status")
	private String status;
	
	/*@Column(name="userId")
	private Long userId;*/
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PatientRequest() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getHospitalNameAddress() {
		return hospitalNameAddress;
	}

	public void setHospitalNameAddress(String hospitalNameAddress) {
		this.hospitalNameAddress = hospitalNameAddress;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmailId() {
		return contactEmailId;
	}

	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
