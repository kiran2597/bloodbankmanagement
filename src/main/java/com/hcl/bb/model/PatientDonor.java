package com.hcl.bb.model;

import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;


@Entity
@Table(name = "patientDonor")
public class PatientDonor implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "donorId")
	private Long id;
	
	@Column(name = "Time")
	private String time;
	
	@Column(name = "glucoseLevel", length = 10, nullable = false)
	@NotEmpty(message="Glucose level shouldn't empty")
	private String glucoseLevel;
	
	@Column(name = "notes", length = 30, nullable = false)
	@NotEmpty(message="Notes shouldn't empty")
	private String notes;
	
	@Column(name="status")
	private String status;
	

	

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PatientDonor() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getGlucoseLevel() {
		return glucoseLevel;
	}

	public void setGlucoseLevel(String glucoseLevel) {
		this.glucoseLevel = glucoseLevel;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}


	
	
}
