package com.hcl.bb.dao;

import java.util.List;
import java.util.Optional;

import com.hcl.bb.model.User;

public interface UserDao {

	public abstract boolean addUser(User user);
	public abstract User findUserByName(String name);
	public abstract boolean checkLogin(String username,String password);
	public abstract List<User> listAllUsers();
	public User findByEmail(String email);
	public User findByResetToken(String resetToken);
	public abstract boolean update(User user);
	
}
