package com.hcl.bb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;


@Repository("adminDao")
public class AdminDaoImpl implements AdminDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PatientRequest> getAllRequests() {
			
		String nql="select * from patientrequest";
		List<PatientRequest> patientRequests=null;
		PatientRequest newPatientRequest = null;
		if(nql!=null) {
			
			Session session1 = sessionFactory.getCurrentSession();
			patientRequests= session1.createNativeQuery(nql).addEntity(PatientRequest.class).getResultList();
			//System.out.println(patientRequests.get(0));
			return patientRequests;
		}
		return patientRequests;
	}

	@Override
	public boolean checkAdmin(String name, String password) {
		
		if(name.equals("hcl") && password.equals("Hcl@5678")) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateRequestStatusById(Long id,String status) {
		
		String nql = "update patientrequest set status=:change where requestId=:id";
		if(nql!=null) {
			Session session1 = sessionFactory.getCurrentSession();
			int result = session1.createNativeQuery(nql).setParameter("change", status).setParameter("id", id).addEntity(PatientRequest.class).executeUpdate();
			if(result==1) {
				System.out.println("update status");
				return true;
			}
		}
		return false;
	}

	@Override
	public List<PatientDonor> getAllDonors() {
		
		String nql="select * from patientdonor";
		List<PatientDonor> patientDonors=null;
		if(nql!=null) {
			
			Session session1 = sessionFactory.getCurrentSession();
			 patientDonors = session1.createNativeQuery(nql).addEntity(PatientDonor.class).getResultList();
			 if(patientDonors.isEmpty()) {
				 return null;
			 }
		}
		return patientDonors;
		
	}

	@Override
	public boolean updateDonorStatusById(Long id, String status) {
		
		String nql = "update patientdonor set status=:change where donorId=:id";
		if(nql!=null) {
			Session session1 = sessionFactory.getCurrentSession();
			int result = session1.createNativeQuery(nql).setParameter("change", status).setParameter("id", id).addEntity(PatientDonor.class).executeUpdate();
			if(result==1) {
				System.out.println("update status");
				return true;
			}
		}
		return false;
	}

	


}
