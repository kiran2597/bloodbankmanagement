package com.hcl.bb.dao;

import java.util.List;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;

public interface RequestPatientDao {

	public abstract boolean addRequestForm(PatientRequest patientRequest,String name);
	public abstract List<PatientRequest> getAllRequests(Long userId);
}
