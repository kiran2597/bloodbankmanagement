package com.hcl.bb.dao;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;
import com.hcl.bb.model.User;

@Repository("patientDonorDao")
public class PatientDonorDaoImpl implements PatientDonorDao {


	@Autowired
	private SessionFactory sessionFactory; 
	
	@Autowired
	private UserDao userDao;

	Session session = null;

	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	@Override
	public boolean addDonorForm(PatientDonor donor, String name) {

		User user = userDao.findUserByName(name);
		String nql = "insert into patientdonor(glucoseLevel,notes,Time,userId) values(?,?,?,?)";
		if (donor != null) {
					
			Session session1 = getSession();
			Query query = session1.createNativeQuery(nql).addEntity(PatientDonor.class);
			query.setParameter(1, donor.getGlucoseLevel());
			query.setParameter(2, donor.getNotes());
			query.setParameter(3, donor.getTime());
			query.setParameter(4, user.getId());
			int result = query.executeUpdate();
			if (result == 1) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<PatientDonor> getAllRequests(Long userId) {
		
		String nql="select * from patientdonor where userId= :id";
		List<PatientDonor> patientdonors=null;
		if(nql != null) {
			
			Session session1 = getSession();
			patientdonors= session1.createNativeQuery(nql).setParameter("id", userId).addEntity(PatientDonor.class).getResultList();
			if(patientdonors.isEmpty()) {
				return null;
			}
		}
		return patientdonors;
	}

	@Override
	public List<User> checkBloodStatus() {
		
		String nql = "select * from user where userId IN (select userId from patientdonor where status='Approved')";
		List<User> users = null;
		if(nql !=null) {
			Session session1 = getSession();
			users = session1.createNativeQuery(nql).addEntity(User.class).getResultList();
		}
		return users;
	}

}
