package com.hcl.bb.dao;

import java.util.List;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;

public interface AdminDao {

	public abstract List<PatientRequest> getAllRequests();
	public abstract boolean checkAdmin(String name,String password);
	public abstract boolean updateRequestStatusById(Long id,String status);
	public abstract boolean updateDonorStatusById(Long id,String status);
	public abstract List<PatientDonor> getAllDonors();
}
