package com.hcl.bb.dao;

import java.util.List;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.User;

public interface PatientDonorDao {

	public boolean addDonorForm(PatientDonor donor, String name);
	public abstract List<PatientDonor> getAllRequests(Long userId);
	public abstract List<User> checkBloodStatus();
}
