package com.hcl.bb.dao;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transaction;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.bb.customException.UserDefinedException;
import com.hcl.bb.model.User;


@Repository("userDao")
public class UserDaoImpl implements UserDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);
	
	Session session = null; 	
	
	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		}catch(Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	@Override
	public boolean addUser(User user) {
		
		if (user != null) {
			Session session1 = getSession();
			session1.save(user);
			return true;
		}
		return false;
	}


	@Override
	public boolean checkLogin(String username, String password) {
		return false;
		
	}

	@Override
	public List<User> listAllUsers() {
		
		String hql = "from user";
		List userLists = null;
		Session session1 = getSession();
		if(!hql.isEmpty()) {
			Query query = session1.createQuery(hql);
			userLists = query.list();
			return userLists;
		}
		return userLists;
	}

	@Override
	public User findUserByName(String name) {
		
		Session session1 =getSession();
		String hql = "select * from user p where p.username= :name";
		User user = null;
		if (!hql.isEmpty()) {
			
			List<User> list = session1.createNativeQuery(hql).setParameter("name", name).addEntity(User.class).getResultList();
			user = new User();
			for (User user2 : list) {
				user.setId(user2.getId());
				user.setUserName(user2.getUserName());
				user.setPassword(user2.getPassword());
			}
			System.out.println(user.getId());
			LOGGER.info("User is found by username");			
		}

		return user;
	}

	@Override
	public User findByEmail(String email) {
		
		Session session1=getSession();
		String nql="Select * from user where email=:email";
		 User newUser = null;
		if(!nql.isEmpty()) {
			List<User> userD = session1.createNativeQuery(nql).setParameter("email", email).addEntity(User.class).getResultList();
			newUser = new User();
			for(User us:userD) {
				newUser.setId(us.getId());
				newUser.setEmail(us.getEmail());
				newUser.setResetToken(us.getResetToken());	
				newUser.setPassword(us.getPassword());
			}
		}
		return newUser;
	}

	@Override
	public User findByResetToken(String resetToken) {
		Session session1=getSession();
		String nql="Select * from user where reset_token=:token";
		User newUser = null;
		if(!nql.isEmpty()) {
			List<User> userD= session1.createNativeQuery(nql).setParameter("token", resetToken).addEntity(User.class).getResultList();
			for(User us:userD) {
				newUser.setId(us.getId());
				newUser.setEmail(us.getEmail());
				newUser.setResetToken(us.getResetToken());	
				newUser.setPassword(us.getPassword());
			}
		}
		return newUser;
	}

	@Override
	public boolean update(User user) {
		
		if(user!=null) {
			Session session1=getSession();
			session1.saveOrUpdate(user);
			return true;
		}
		return false;
	}
	

}
