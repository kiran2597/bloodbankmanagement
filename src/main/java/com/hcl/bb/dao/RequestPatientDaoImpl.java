package com.hcl.bb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.bb.model.PatientDonor;
import com.hcl.bb.model.PatientRequest;
import com.hcl.bb.model.User;


@Repository("requestPatientDao")
public class RequestPatientDaoImpl implements RequestPatientDao{

	@Autowired
	private SessionFactory sessionFactory; 
	
	@Autowired
	private UserDao userDao;
	
	Session session = null; 	
	
	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		}catch(Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	@Override
	public boolean addRequestForm(PatientRequest patientRequest,String name) {
		
		User user = userDao.findUserByName(name);
		String nql="insert into patientrequest(bloodGroup, city, contactEmailId, contactName, contactNumber, date, doctorName, hospitalNameAddress, message, patientName,userId) values(?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
		if(patientRequest!=null) {
			Session session1 = getSession();
			Query query =session1.createNativeQuery(nql).addEntity(PatientRequest.class);
			query.setParameter(1, patientRequest.getBloodGroup());
			query.setParameter(2, patientRequest.getCity());
			query.setParameter(3, patientRequest.getContactEmailId());
			query.setParameter(4, patientRequest.getContactName());
			query.setParameter(5, patientRequest.getContactNumber());
			query.setParameter(6, patientRequest.getDate());
			query.setParameter(7, patientRequest.getDoctorName());
			query.setParameter(8, patientRequest.getHospitalNameAddress());
			query.setParameter(9, patientRequest.getMessage());
			query.setParameter(10, patientRequest.getPatientName());			
			query.setParameter(11, user.getId());
			int result =query.executeUpdate();
			if(result == 1) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<PatientRequest> getAllRequests(Long userId) {
		
		System.out.println(userId);
		String nql = "select * from patientrequest where userId=:id";
		List<PatientRequest> patientRequests = new ArrayList<PatientRequest>();
		if(nql != null) {
			session = getSession();
			patientRequests = session.createNativeQuery(nql).setParameter("id", userId).addEntity(PatientRequest.class).getResultList();
			if(patientRequests.isEmpty()) {
				return null;
			}
		}
		return patientRequests;
	}


	
	
}
