<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 <%@ include file="/nav.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Check Availability</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
	<table class="table table-striped">
			<c:if test="${requestScope.bloodAvailUsers == null}">
					<c:out value="There is no records for requesting blood"/>
			</c:if>
			<c:if test="${requestScope.bloodAvailUsers != null}">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">FirstName</th>
						<th scope="col">LastName</th>
						<th scope="col">Blood Group</th>
						<th scope="col">City</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${requestScope.bloodAvailUsers}">
						<tr>
							<td>${user.id}</td>
							<td>${user.firstName}</td>
							<td>${user.lastName}</td>
							<td>${user.bloodGroup}</td>
							<td>${user.city}</td>
						</tr>
					</c:forEach>
				</tbody>
			</c:if>
		</table>
	</div>
</body>
</html>