<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/nav.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<title>Requester page</title>
<style>
body {
	​​​​​​ margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}
.error{color: red}
#phoneNumberError {
	​​ font-size: 15px;
	text-align: center;
	color: red;
}
​
</style>

<script type="text/javascript">
	function phoneNumberCheck()
	{​​
	var phone = document.getElementById("contactNumber").value;
	var filter = /^[0-9]+$/;
	if (phone.length == 0 ) {​​
	document.getElementById("phoneNumberError").innerHTML ="PhoneNumber should not empty";
	document.my-form.phone.focus();
	return false;
	}​​
	else if(phone.length!=10 || !filter.test(phone)){​​
	document.getElementById("phoneNumberError").innerHTML ="Enter only 10 Digit mobile number";
	document.my-form.phone.focus();
	return false;
	}​​
​​
</script>
</head>
<body>
	<center>
		<div class="container">
			<div id="donar" style="margin-top: 70px"
				class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">Requester Information</div>

					</div>
					<div class="panel-body">
						<form:form action="./requestform" class="form-horizontal"
							method="post" role="form" modelAttribute="requestData">

							<div id="patientName" class="form-group required">
								<label for="patientName"
									class="control-label col-md-4  requiredField">Patient
									Name </label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="patientName"
										name="patientName" placeholder="Patient's Full Name"
										style="margin-bottom: 10px" type="text" path="patientName"
										/>
									<form:errors path="patientName" cssClass="error"></form:errors>
								</div>
							</div>


							<div id="bloodGroup" class="form-group required">
								<label for="bloodGroup"
									class="control-label col-md-4  requiredField">Required
									Blood Group </label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="bloodGroup"
										name="bloodGroup" placeholder="Required Blood Group"
										style="margin-bottom: 10px" type="text" path="bloodGroup"
										/>
									<form:errors path="bloodGroup" cssClass="error"></form:errors>
								</div>
							</div>


							<div id="city" class="form-group required">
								<label for="city" class="control-label col-md-4  requiredField">City
								</label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="city" name="city"
										placeholder="Enter City" style="margin-bottom: 10px"
										type="text" path="city" />
									<form:errors path="city" cssClass="error"></form:errors>
								</div>
							</div>

							<div id="doctorName" class="form-group required">
								<label for="doctorName"
									class="control-label col-md-4  requiredField">Doctor's
									Name </label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="doctorName"
										name="doctorName" placeholder="Enter Doctor's  Full Name"
										style="margin-bottom: 10px" type="text" path="doctorName"
										/>
								    <form:errors path="doctorName" cssClass="error"></form:errors>
								</div>
							</div>

							<div id="hospitalNameAddress" class="form-group required">
								<label for="hospitalNameAddress"
									class="control-label col-md-4  requiredField">Hospital
									name and Address </label>

								<div class="controls col-md-8 ">
									<form:textarea class=" form-control" id="hospitalNameAddress"
										name="hospitalNameAddress"
										placeholder="Enter Hospital Name and Address"
										style="margin-bottom: 10px" type="text"
										path="hospitalNameAddress" rows="5" cols="37"
										/>
								    <form:errors path="hospitalNameAddress" cssClass="error"></form:errors>
								</div>
							</div>

							<div id="date" class="form-group required">
								<label for="date" class="control-label col-md-4  requiredField">Required
									Date </label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="date" name="date"
										placeholder="Date on which blood is required"
										style="margin-bottom: 10px" type="date" path="date"
										 />
									  <form:errors path="date" cssClass="error"></form:errors>
								</div>
							</div>

							<div id="contactName" class="form-group required">
								<label for="date" class="control-label col-md-4  requiredField">Contact
									Name </label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="contactName"
										name="contactName"
										placeholder="The name of the person to contact"
										style="margin-bottom: 10px" type="text" path="contactName"
										 />
									<form:errors path="contactName" cssClass="error"></form:errors>
								</div>
							</div>


							<div id="contactNumber" class="form-group required">
								<label for="date" class="control-label col-md-4  requiredField">Contact
									Number</label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="contactNumber"
										name="contactNumber"
										placeholder="The phone number of person to contact"
										style="margin-bottom: 10px" type="text" path="contactNumber"
										onblur="phoneNumberCheck()" />
									<form:errors path="contactNumber" cssClass="error"></form:errors>
								</div>
							</div>

							<div id="contactEmailId" class="form-group required">
								<label for="date" class="control-label col-md-4  requiredField">Contact
									Email ID</label>

								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="contactEmailId"
										name="contactEmailId"
										placeholder="The email id of person to contact"
										style="margin-bottom: 10px" type="text" path="contactEmailId"
										onblur="phoneNumberCheck()" />
									<form:errors path="contactEmailId" cssClass="error"></form:errors>
								</div>
								
							</div>
							<div id="message" class="form-group required">
								<label for="date" class="control-label col-md-4  requiredField">Message</label>

								<div class="controls col-md-8 ">
									<form:textarea class=" form-control" id="message"
										name="message" placeholder="Custom Message"
										style="margin-bottom: 10px" type="text" path="message"
										 />
									<form:errors path="message" cssClass="error"></form:errors>
								</div>
							</div>
							<div class="form-group">
								<div class="aab controls col-md-3 "></div>
								<div class="controls col-md-8 ">
									<input type="submit" name="Save" value="Save"
										class="btn btn-primary btn btn-primary" id="submit-id-save" />
									<input type="reset" name="cancel" value="Cancel"
										class="btn btn btn-primary" id="button-id-cancel" />
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</center>

</body>
</html>