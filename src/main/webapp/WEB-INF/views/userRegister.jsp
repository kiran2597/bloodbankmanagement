<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link href="<c:url value="/resources/css/register.css" />" rel="stylesheet" />
<script type="text/javascript"  src="<c:url value="/resources/js/register.js" />"></script>	

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
	<div class="container">
		<a class="navbar-brand" href="#">BloodBank</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>


		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" href="./login">Login</a></li>
				<li class="nav-item"><a class="nav-link" href="./add">Register</a>
				</li>
			</ul>


		</div>
	</div>
	</nav>
	<main class="my-form">
	<div class="cotainer">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Register</div>
					<div class="card-body">

						<form:form action="./register" name="my-form" method="post"
							modelAttribute="newUser">
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">FirstName</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" path="firstName"
										id="firstName" placeholder="Enter your FirstName" />
									<form:errors style="color:red" path="firstName"></form:errors>
									<span id="firstNameError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">LastName</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="lastName"
										path="lastName" placeholder="Enter your LastName"
										minlength="6" onblur="lastNameCheck()"  />
									<form:errors style="color:red" path="lastName"></form:errors>
									<span id="lastNameError"></span>
								</div>
							</div>

							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">UserName</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="userName"
										path="userName" placeholder="Enter your UserName"
										minlength="6" onblur="userNameCheck()" />
									<form:errors style="color:red" path="userName"></form:errors>
									<span id="userNameError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">BloodGroup</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="bloodGroup"
										path="bloodGroup" placeholder="Enter your BloodGroup"
										minlength="2" onblur="bloodGroupCheck()"  />
									<form:errors style="color:red" path="bloodGroup"></form:errors>
									<span id="bloodGroupError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">City</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="city"
										path="city" placeholder="Enter your City" minlength="4"
										onblur="cityCheck()" />
									<form:errors style="color:red" path="city"></form:errors>
									<span id="cityError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="email" 
								class="col-md-4 col-form-label text-md-right">Email</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="email"
									path="email" placeholder="Enter email address" onblur="emailCheck()"/>
									<form:errors style="color:red" path="email"></form:errors>
									<span id="emailError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">PhoneNumber</label>
								<div class="col-md-6">
									<form:input type="text" class="form-control" id="phoneNumber"
										path="phoneNumber" placeholder="Enter PhoneNumber"
										onblur="phoneNumberCheck()" />
									<form:errors style="color:red" path="phoneNumber"></form:errors>
									<span id="phoneNumberError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Password</label>
								<div class="col-md-6">
									<form:input type="password" class="form-control" id="pwd"
										path="password" placeholder="Enter password" minlength="6"
										onblur="passwordCheck()" />
									<form:errors style="color:red" path="password"></form:errors>
									<span id="passwordError"></span>
								</div>
							</div>
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">Register
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</main>


</body>
</html>