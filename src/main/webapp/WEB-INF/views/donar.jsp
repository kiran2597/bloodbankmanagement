<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/nav.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/nav.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T">
<title>Donar page</title>
<style>
body {
	margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
	<center>
		<div class="container">
			<div id="donar" style="margin-top: 70px"
				class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">Donar Information</div>
					</div>
					<div class="panel-body">
						<form:form action="./donorform" class="form-horizontal" method="post"
							role="form" modelAttribute="donarData">
							<div id="time" class="form-group required">
								<label for="time" class="control-label col-md-4  requiredField">Time
								</label>
								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="time" name="Time"
										placeholder="Enter the Time" style="margin-bottom: 10px"
										type="time" path="time" />
								</div>
							</div>
							<div id="bloodglucoselevel" class="form-group required">
								<label for="blood_glucose-level"
									class="control-label col-md-4  requiredField">Blood
									Glucose Level </label>
								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="bloodglucoselevel"
										name="glucoselevel" placeholder="Enter Blood Glucose Level"
										style="margin-bottom: 10px" type="text"
										path="glucoseLevel" />
								</div>
							</div>
							<div id="notes" class="form-group required">
								<label for="notes" class="control-label col-md-4  requiredField">Notes
								</label>
								<div class="controls col-md-8 ">
									<form:input class=" form-control" id="notes" name="Notes"
										placeholder="Enter Note" style="margin-bottom: 10px"
										type="text" path="notes" />
								</div>
							</div>
							<div class="form-group">
								<div class="aab controls col-md-3 "></div>
								<div class="controls col-md-8 ">
									<input type="submit" name="Save" value="Save"
										class="btn btn-primary btn btn-primary" id="submit-id-save" />
									<input type="reset" name="cancel" value="Cancel"
										class="btn btn btn-primary" id="button-id-cancel" />
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</center>
</body>
</html>