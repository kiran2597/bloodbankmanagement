<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/nav.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
.indented {
  padding-left: 50pt;
  padding-right: 50pt;
}
.subdiv {
position: absolute;
bottom: 0px;
}
.example1 {
 height: 50px;	
 overflow: hidden;
 position: relative;
}
.example1 h3 {
 font-size: 1.5em;
 color: #E15D44;
 position: absolute;
 width: 100%;
 height: 100%;
 margin: 0;
 line-height: 50px;
 text-align: center;
 -moz-transform:translateX(100%);
 -webkit-transform:translateX(100%);	
 transform:translateX(100%);	
 -moz-animation: example1 15s linear infinite;
 -webkit-animation: example1 15s linear infinite;
 animation: example1 15s linear infinite;
}
@-moz-keyframes example1 {
 0%   { -moz-transform: translateX(100%); }
 100% { -moz-transform: translateX(-100%); }
}
@-webkit-keyframes example1 {
 0%   { -webkit-transform: translateX(100%); }
 100% { -webkit-transform: translateX(-100%); }
}
@keyframes example1 {
 0%   { 
 -moz-transform: translateX(100%); /* Firefox bug fix */
 -webkit-transform: translateX(100%); /* Firefox bug fix */
 transform: translateX(100%); 		
 }
 100% { 
 -moz-transform: translateX(-100%); /* Firefox bug fix */
 -webkit-transform: translateX(-100%); /* Firefox bug fix */
 transform: translateX(-100%); 
 }
}
</style>
<title>Home</title>
</head>
<body>

		
<div class="example1">
<h3>Welcome to Blood Bank Management System</h3>
</div>
	<div>
		<p class="indented">Blood banking refers to the process of
			collecting, separating, and storing blood. The first U.S. blood bank
			was established in 1936. Today, blood banks collect blood and
			separate it into its various components so they can be used most
			effectively according to the needs of the patient. Red blood cells
			carry oxygen, platelets help the blood clot, and plasma has specific
			proteins that allow proper regulation of coagulation and healing.
			Although research has yielded drugs that help people's bone marrow
			produce new blood cells more rapidly, the body's response time can
			still take weeks, thus donated blood remains an important and more
			immediate life-saving resource. Blood is the vital connection to
			having a healthy body, and according to the American Red Cross,
			nearly 5 million people receive blood transfusions each year. Thanks
			to years of research, much progress has been made towards making
			transfusions safer and more effective.</p>
	</div>

</body>
</html>