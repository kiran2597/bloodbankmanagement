<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 <%@ include file="/navHome.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Requests</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <script>
 		function myfunction1(){
 				document.querySelector('#approve').innerHTML = 'Approved';
 				document.getElementById("approve").disabled = true;
 				document.getElementById("reject").disabled = true;
 		}
 		function myfunction2(){
 				document.querySelector('#reject').innerHTML = 'Rejected';
				document.getElementById("reject").disabled = true;
				document.getElementById("approve").disabled = true;
 		}
 </script>
</head>
<body>
	<div class="container">
	<table class="table table-striped">
			<c:if test="${requestScope.patientRequests == null}">
					<c:out value="There is no records for requesting blood"/>
			</c:if>
			<c:if test="${requestScope.patientRequests != null}">
				<thead>
					<tr>
						<th scope="col">Request Id</th>
						<th scope="col">PatientName</th>
						<th scope="col">BloodGroup</th>
						<th scope="col">City</th>
						<th scope="col">DoctorName</th>
						<th scope="col">HospitalName and Address</th>
						<th scope="col">Date of requirement</th>
						<th scope="col">ContactName</th>
						<th scope="col">ContactNumber</th>
						<th scope="col">ContactEmailId</th>
						<th scope="col">Message</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="patient" items="${requestScope.patientRequests}">
						<tr>
							<td>${patient.id}</td>
							<td>${patient.patientName}</td>
							<td>${patient.bloodGroup}</td>
							<td>${patient.city}</td>
							<td>${patient.doctorName}</td>
							<td>${patient.hospitalNameAddress}</td>
							<td>${patient.date}</td>
							<td>${patient.contactName}</td>
							<td>${patient.contactNumber}</td>
							<td>${patient.contactEmailId}</td>
							<td>${patient.message}</td>
							<td>
								<c:set var="status" value="In Progress" />
								<c:set var="status1" value="Approved" />
								<c:set var="status2" value="Rejected" />
								<c:if test="${patient.status == status}">
									<form action="./approvestatus/${patient.id}" method="post" onsubmit="myfunction1()">
										<input type="submit" class="btn btn-success" id="approve" value="Approve" />	
									</form>
									<form action="./rejectstatus/${patient.id}" method="post" onsubmit="myfunction2()">
										<input type="submit" class="btn btn-danger" id="reject" value="Reject" />
									</form>
								</c:if>
								<c:if test="${ patient.status == status1}">
									<button class="btn btn-success">Approved</button>
								</c:if>
								<c:if test="${ patient.status == status2}">
									<button class="btn btn-danger">Rejected</button>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</c:if>
		</table>
	</div>
</body>
</html>