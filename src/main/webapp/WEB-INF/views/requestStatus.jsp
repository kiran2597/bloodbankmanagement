<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 <%@ include file="/nav.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Status</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
	<table class="table table-striped">
			<c:if test="${requestScope.requests == null}">
					<c:out value="There is no records for requesting blood"/>
			</c:if>
			<c:if test="${requestScope.requests != null}">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">PatientName</th>
						<th scope="col">BloodGroup</th>
						<th scope="col">Date</th>
						<th scope="col">Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="patient" items="${requestScope.requests}">
						<tr>
							<td>${patient.id}</td>
							<td>${patient.patientName}</td>
							<td>${patient.bloodGroup}</td>
							<td>${patient.date}</td>
							<td>${patient.status}</td>
						</tr>
					</c:forEach>
				</tbody>
			</c:if>
		</table>
	</div>
</body>
</html>