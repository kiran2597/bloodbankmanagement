<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 <%@ include file="/navHome.html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Donors</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
	<table class="table table-striped">
			<c:if test="${requestScope.patientDonors == null}">
					<c:out value="There is no records for donating blood"/>
			</c:if>
			<c:if test="${requestScope.patientDonors != null}">
			<thead>
				<tr>
					<th scope="col">Donor Id</th>
					<th scope="col">Glucose Level</th>
					<th scope="col">Notes</th>
					<th scope="col">Time</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
					<c:set var="status" value="In Progress" />
					<c:set var="status1" value="Approved" />
					<c:set var="status2" value="Rejected" />
					<c:forEach var="patient" items="${requestScope.patientDonors}">
					 <tr>
						<td>${patient.id}</td>
						<td>${patient.glucoseLevel}</td>
						<td>${patient.time}</td>
						<td>${patient.notes}</td>
						<td>
								<c:if test="${patient.status == status}">
									<form action="./approvednstatus/${patient.id}" method="post" onsubmit="myfunction1()">
										<input type="submit" class="btn btn-success" id="approve" value="Approve" />	
									</form>
									<form action="./rejectdnstatus/${patient.id}" method="post" onsubmit="myfunction2()">
										<input type="submit" class="btn btn-danger" id="reject" value="Reject" />
									</form>
								</c:if>
								<c:if test="${ patient.status == status1}">
									<button class="btn btn-success">Approved</button>
								</c:if>
								<c:if test="${ patient.status == status2}">
									<button class="btn btn-danger">Rejected</button>
								</c:if>						
						</td>
					  </tr>
					</c:forEach>
			
			</tbody>
			</c:if>
	</table>
	</div>
</body>
</html>