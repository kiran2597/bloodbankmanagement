<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">	
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<title>ForgotPassword Page</title>
<style type="text/css">
.container{
	padding-top: 100px;
}

</style>
<script type="text/javascript">
function emailCheck(){
	var email = document.getElementById("email").value;
	var regex = /\S+@\S+\.\S+/;
	if(email == "" || !regex.test(email)){
		document.getElementById("emailError").innerHTML = "Email should not be empty and in specific format";
		document.my-form.email.focus();
		return false;
	}
}
</script>
</head>
<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<div class="card">
				  <div class="card-header">Forgot Password</div>
					<div class="card-body">
						<form action="./forgot" method="post">
							<div class="form-group-row">
								<label class="col-md-12 text-md-left col-form-label">Enter your email address and we'll send link to reset your password</label><br>
									<input type="text" class="form-control" placeholder="Email address"
									onblur="emailCheck()" name="email" id="email"/>
									<span style="color: red" id="emailError"></span>	
							</div><br>
							<div class="col-md-6 offset-md-4">
								<input type="submit" class="btn btn-primary" value="Submit"/> 
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>