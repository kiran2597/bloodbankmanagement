<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600"
	rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link href="<c:url value="/resources/css/register.css" />" rel="stylesheet" />
<script type="text/javascript"  src="<c:url value="/resources/js/register.js" />"></script>	
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
	<div class="container">
		<a class="navbar-brand" href="#">BloodBank</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/admin">Admin Login</a></li>
				<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/user/login">Login</a></li>
				<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/user/add">Register</a>
				</li>
			</ul>
		</div>
	</div>
	</nav>
	<main class="my-form">
	<div class="cotainer">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">Login</div>
					<div class="card-body">
						<c:if test="${not empty error}">
							<p align="center" style="color:red">${error}</p>
						</c:if>
						<form:form action="./authenticate" name="myform" method="post" modelAttribute="userLogin">
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Username</label>
								<div class="col-md-5">
									<form:input type="text" class="form-control" path="username"
										id="username" placeholder="Enter username"
										onblur="userNamecheck()" />
									<form:errors style="color:red" path="username"></form:errors>
									<span id="nameError"></span>
								</div>
							</div>
							<div class="form-group row">
								<label for="full_name"
									class="col-md-4 col-form-label text-md-right">Password</label>
								<div class="col-md-5">
									<form:input type="password" class="form-control" id="password"
										path="password" placeholder="Enter password" minlength="6"
										onblur="CheckPassword()" />
									<form:errors style="color:red" path="password"></form:errors>
									<span id="passwordError"></span>
								</div>
							</div>
							<div class="col-md-6 offset-md-4">
								<p style="align:center;"><a href="./forgot">Forget Password?</a></p>
							</div>
							<div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">Login
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</main>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script
src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script
src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		</body>
		</html>
	