function firstNameCheck() {
	var name = document.getElementById("firstName").value;
	if (name == "" || name == null) {
		document.getElementById("firstNameError").innerHTML = "FirstName should not be empty";
		document.my-form.name.focus();
		return false;
	}
}
function lastNameCheck() {
	var name = document.getElementById("lastName").value;
	if (name == "" || name == null) {
		document.getElementById("lastNameError").innerHTML = "LastName should not be empty";
		document.my-form.name.focus();
		return false;
	}
}
function userNameCheck() {
	var name = document.getElementById("userName").value;
	if (name == "" || name == null) {
		document.getElementById("userNameError").innerHTML = "UserName should not be empty";
		document.my-form.name.focus();
		return false;
	}
}

function emailCheck(){
	var email = document.getElementById("email").value;
	var regex = /\S+@\S+\.\S+/;
	if(email == "" || !regex.test(email)){
		document.getElementById("emailError").innerHTML = "Email should not be empty and in specific format";
		document.my-form.email.focus();
		return false;
	}
}


function bloodGroupCheck() {
	var blood = document.getElementById("bloodGroup").value;
	if (blood == "" || blood == null) {
		document.getElementById("bloodGroupError").innerHTML = "BloodGroup should not be empty";
		document.my-form.blood.focus();
		return false;
	}
}

function cityCheck() {
	var city = document.getElementById("city").value;
	if (city == "" || city == null) {
		document.getElementById("cityError").innerHTML = "CityName should not be empty";
		document.my-form.city.focus();
		return false;
	}
}
function passwordCheck() {
	var password = document.getElementById("pwd").value;
	var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	if (password.length == 0) {
		document.getElementById("passwordError").innerHTML = "Password should not empty";
		document.my-form.pwd.focus();
		return false;
	} else if (password.length < 6 || !passw.test(password)) {
		document.getElementById("passwordError").innerHTML = "Password should have atleast one captial,special character";
		document.my-form.pwd.focus();
		return false;
	}
}
function phoneNumberCheck() {
	var phone = document.getElementById("phoneNumber").value;
	var filter = /^[0-9]+$/;
	if (phone.length == 0) {
		document.getElementById("phoneNumberError").innerHTML = "PhoneNumber should not empty";
		document.my-form.phone.focus();
		return false;
	} else if (phone.length != 10 || !filter.test(phone)) {
		document.getElementById("phoneNumberError").innerHTML = "Enter only 10 Digit mobile number";
		document.my-form.phone.focus();
		return false;
	}
}
